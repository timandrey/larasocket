<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Follower extends Model
{
    public static function getFollowers($post_id, $type)
    {
        $users = [];
        $followers = Follower::where('type', $type)->where('post_id', $post_id)->get();
        foreach ($followers as $key=>$follower){
            $users[$key] = User::find($follower->follower_id);
        }
        return $users;
    }

    public static function getFollower($user_id, $post_id)
    {
        return Follower::where('follower_id', $user_id)->where('post_id', $post_id);
    }

    public static function isFollower($user_id, $type, $post_id)
    {
        if (Follower::where('follower_id', $user_id)
            ->where('type', $type)
            ->where('post_id', $post_id)
            ->first()) {
            return true;
        } else {
            return false;
        }
    }
}
