<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Conversation extends Model
{
    public static function getConversationInfo($conversation)
    {
        if ($conversation->title) {
            $conversation->title_class = 'with-title';
        } else {
            $conversation->title_class = 'without-title';
            $conversation->title       = 'Без темы';
        }
        $conversation->messages = Message::where('conversation_id', $conversation->id)->get();
        foreach($conversation->messages as $message){
            $message->author     = User::find($message->author_id)->name;
            $message->class      = ($message->author_id == Auth::user()->id) ? 'message-item-my' : 'message-item-other';
        }
        $conversation->followers   = Follower::getFollowers($conversation->id, 'conversation');
        $conversation->is_follower = Follower::isFollower(Auth::user()->id, 'conversation', $conversation->id);
        $conversation->users       = User::all();
        foreach ($conversation->users as $user) {
            $user->followed_conversation = Follower::isFollower($user->id, 'conversation', $conversation->id);
        }
        return $conversation;
    }
}
