<?php


namespace App\Console\Commands;

use Bunny\Async\Client as AsyncClient;
use Bunny\Channel;
use Bunny\Message;
use Illuminate\Console\Command;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use App\Classes\Socket\Pusher;
use React\EventLoop\Factory as ReactLoop;
use React\ZMQ\Context as ReactContext;
use React\Socket\Server as ReactServer;
use Ratchet\Wamp\WampServer;

class WebSocketServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'push_server:serve';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push server started';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $loop    = ReactLoop::create();
        $pusher  = new Pusher;
        $queueAsyncClient = new AsyncClient($loop, [
            "host"     => $_ENV['RABBITMQ_HOST'],
            "port"     => $_ENV['RABBITMQ_PORT'],
            "vhost"    => $_ENV['RABBITMQ_VHOST'],
            "user"     => $_ENV['RABBITMQ_USERNAME'],
            "password" => $_ENV['RABBITMQ_PASSWORD'],
        ]);

        $connect = $queueAsyncClient->connect();
        $connect->then(function (AsyncClient $client) {
            return $client->channel();
        })->then(function (Channel $channel) use ($pusher) {
            $channel->queueDeclare('updatemessage');
            $channel->consume(
                function (Message $message) use ($pusher, $channel) {
                    $pusher->sendDataToServer($message->content);
                },
                'updatemessage',
                '',
                false,
                true
            );
        });

        $context = new ReactContext($loop);

        $pull = $context->getSocket(\ZMQ::SOCKET_PULL);
        $pull->bind('tcp://127.0.0.1:5555');
        $pull->on('message', [$pusher, 'broadcast']);

        $web_sock = new ReactServer('tcp://0.0.0.0:8000', $loop);
        $web_server = new IoServer(
            new HttpServer(
                new WsServer(
                    new WampServer($pusher)
                )
            ),
            $web_sock
        );
        $this->info('Push server is running');
        $loop->run();
    }
}