<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmailJob;
use App\Mail\ConversationMention;
use App\Mail\MessageCreated;

class MailController extends Controller
{
    public static function conversationMentionMessage($email, $conversation, $recipient_id)
    {
        $params = [
            'conversation' => $conversation,
            'recipient_id' => $recipient_id
        ];
        SendEmailJob::dispatch($email, new ConversationMention($params));
       // Mail::to($email)->send(new ConversationMention($params));
    }
    public static function messageCreatedMessage($email, $conversation, $recipient_id)
    {
        $params = [
            'conversation' => $conversation,
            'recipient_id' => $recipient_id
        ];
        SendEmailJob::dispatch($email, new MessageCreated($params));
      //  Mail::to($email)->send(new MessageCreated($params));
    }
}
