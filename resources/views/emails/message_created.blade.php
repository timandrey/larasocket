@component('mail::message')
    # Новое сообщение в беседе

    В беседе "{{$conversation->title}}" добавлено новое сообщение

    <a href="{{url('/').'/conversations/'.$conversation->id}}">Посмотреть беседу</a>
@endcomponent