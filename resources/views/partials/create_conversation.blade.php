<input type="text" class="form-control" placeholder="Тема обсуждения" id="new-conversation-title">
<label for="new-conversation-message" class="for-textarea">
    <div id="new-conversation-editor">
        <textarea id="new-conversation-message" rows="1" class="form-control"></textarea>
    </div>
</label>
<button class="btn btn-primary" id="new-conversation-submit">Отправить</button>
<div class="dialog-hover-block float-right conversation-button">
    <span id="new-conversation-mention">
        <i class="fa fa-at" aria-hidden="true"></i>
    </span>
    <ul class="dialog new-conversation-mention-dialog">
        @foreach($users as $user)
            <li data-name="{{$user->name}}">{{$user->name}}</li>
        @endforeach
    </ul>
</div>