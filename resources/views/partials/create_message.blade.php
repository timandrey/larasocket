<div id="message-editor-{{$conversation->id}}">
    <textarea id="new-message-{{$conversation->id}}" rows="1" class="form-control message-textarea"></textarea>
</div>
<button class="btn btn-primary message-submit" id="message-submit-{{$conversation->id}}" data-conversation_id="{{$conversation->id}}">
    Отправить
</button>
<div class="dialog-hover-block float-right conversation-button">
    <span class="message-mention-button" id="message-mention-button-{{$conversation->id}}"
          data-conversation_id="{{$conversation->id}}">
        <i class="fa fa-at" aria-hidden="true"></i>
    </span>
    <ul class="dialog mention-dialog" id="mention-dialog-{{$conversation->id}}">
        @foreach($users as $user)
            <li data-name="{{$user->name}}" data-conversation_id="{{$conversation->id}}">{{$user->name}}</li>
        @endforeach
    </ul>
</div>