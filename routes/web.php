<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/conversations/{conversation_id?}', 'ConversationController@index')->name('conversations');
Route::get('/createConversationAjax/', 'ConversationController@create')->name('createConversationAjax');

Route::get('/deleteConversationAjax/', 'ConversationController@delete')->name('deleteConversationAjax');
Route::get('/updateConversationAjax/', 'ConversationController@update')->name('updateConversationAjax');
Route::get('/sendMessageAjax/', 'ConversationController@sendMessageAjax')->name('sendMessageAjax');
Route::get('/updateFollowerAjax/', 'ConversationController@updateFollowerAjax')->name('updateFollowerAjax');
